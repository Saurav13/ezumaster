<?php

namespace App\Http\Middleware;

use App\MyBroker;
use Closure;
use Auth;

class SSOCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // dd(Auth::user());
        if (Auth::guest()) {
            $broker  = new MyBroker();
            $broker->loginCurrentUser();
        }
        // else{
        //     $broker  = new MyBroker();
        //     dd($broker->checklogin());
        // }
        return $next($request);
    }
}
