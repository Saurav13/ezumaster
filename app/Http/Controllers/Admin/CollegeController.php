<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\College;

class CollegeController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
        // $this->middleware(function ($request, $next) {

        //     if (Auth::user()->roles()->where('title', '=', 'Dashboard')->exists()){
        //         return $next($request);
        //     }
        //     else
        //         abort(403);
        // });
    }
    
    public function index()
    {
        $colleges = College::orderBy('updated_at','desc')->paginate(20);
        return view('admin.colleges')->with('colleges',$colleges);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:191'
        ]);

        $college = new College;
        $college->name = $request->name;
        $college->save();

        $request->session()->flash('success', 'College name sucessfully created');

        return redirect()->route('colleges.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:191'
        ]);

        $college = College::findOrFail($id);
        $college->name = $request->name;
        $college->save();

        $request->session()->flash('success', 'College name sucessfully edited');

        return redirect()->route('colleges.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        College::destroy($id);

        $request->session()->flash('success', 'College name sucessfully deleted');

        return redirect()->route('colleges.index');
    }
}
