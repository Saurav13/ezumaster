<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;

class CategoryController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
        // $this->middleware(function ($request, $next) {

        //     if (Auth::user()->roles()->where('title', '=', 'Dashboard')->exists()){
        //         return $next($request);
        //     }
        //     else
        //         abort(403);
        // });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::orderBy('updated_at','desc')->paginate(20);
        return view('admin.categories')->with('categories',$categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:191'
        ]);

        $category = new Category;
        $category->name = $request->name;
        $category->save();

        $request->session()->flash('success', 'Category sucessfully created');

        return redirect()->route('categories.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:191'
        ]);

        $category = Category::findOrFail($id);
        $category->name = $request->name;
        $category->save();

        $request->session()->flash('success', 'Category sucessfully edited');

        return redirect()->route('categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        Category::destroy($id);

        // ClassHelper::resetDrive($class); for all

        $request->session()->flash('success', 'Category sucessfully deleted');

        return redirect()->route('categories.index');
    }
}
