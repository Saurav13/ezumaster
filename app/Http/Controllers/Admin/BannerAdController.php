<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BannerAd;
use App\Category;
use App\Faculty;
use App\Drive;
use App\Drive_ad;
use Carbon\Carbon;

class BannerAdController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ads = BannerAd::orderBy('updated_at','desc')->paginate(10);
        $cats = Faculty::orderBy('name','asc')->get(); //only if it has class
        $categories = [];
        
        foreach($cats as $c){
            $category['cat'] = $c;

            $all_ids=Drive::where('faculty_id',$c->id)->pluck('id');
            $used_ids=Drive_ad::pluck('id');
            $free_ids=$all_ids->diff($used_ids);

            $category['count'] = $free_ids->count();
            // $category['count'] = $c->classes()->where('banner_ad_id',null)->count();
            $categories []= $category;
            
        }
        
        return view('admin.bannerads.index')->with('ads',$ads)->with('categories',$categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'title'=>'required',
            'category' => 'required|exists:faculties,id'
        ]);

        $all_ids=Drive::where('faculty_id',$request->category)->pluck('id');
        $used_ids=Drive_ad::pluck('id');
        $free_ids=$all_ids->diff($used_ids);
        // $max_qty = Category::find($request->category)->classes->count();
        $max_qty = $free_ids->count();

        $request->validate([
            
            'quantity' => 'required|numeric|min:1|max:'.$max_qty,
            'link' => 'required|url',
            'expiry_date' => 'required|date|after:today',
            'image' => 'required|image'
        ]);
        
        $ad = new BannerAd;
        $ad->title=$request->title;
        $ad->faculty_id = $request->category;
        $ad->quantity = $request->quantity;
        $ad->link = $request->link;
        $ad->expiry_date = $request->expiry_date;

        if($request->hasFile('image')){
            $photo = $request->file('image');
            $filename = $photo->getClientOriginalName();
            $location = public_path('bannerad_images/');
            $photo->move($location,$filename);
            $ad->image = $filename;
        }
        $ad->save();
        $drive_ids=Drive::where('faculty_id',$request->category)->pluck('id');
        $used_ids=Drive_ad::all()->pluck('id');

        $diff=$drive_ids->diff($used_ids)->shuffle()->take($ad->quantity);
        
        foreach($diff as $d)
        {
            $drive_ad= new Drive_ad;
            $drive_ad->id=$d;
            $drive_ad->banner_ads_id=$ad->id;
            $drive_ad->save();
        }

        

        /*
            $classes = Drive::where('category_id',$ad->category_id)->where('banner_ad_id',null)->random($quantity); //with null ad_id

            foreach($classes as $class){
                $class->banner_ad_id = $ad->id;
                $class->save();
            }
        */

        $request->session()->flash('success', 'Banner Ad successfully added.');        
        
        return redirect()->route('bannerAds.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ad = BannerAd::findOrFail($id);

        $cats = Faculty::orderBy('name','asc')->get(); //only if it has class
        $categories = [];
        
        foreach($cats as $c){
            $category['cat'] = $c;

            $all_ids=Drive::where('faculty_id',$c->id)->pluck('id');
            $used_ids=Drive_ad::pluck('id');
            $free_ids=$all_ids->diff($used_ids);

            $category['count'] = $free_ids->count();
            // $category['count'] = $c->classes()->where('banner_ad_id',null)->count();
            $categories []= $category;
            
        }
        
        return view('admin.bannerads.edit')->with('ad',$ad)->with('categories',$categories);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'title'=>'required',
            'link' => 'required|url',
            'expiry_date' => 'required|date|after:today',
            'image' => 'nullable|mimes:jpeg,jpg,png,gif|max:100000'
        ]);
        
        $ad = BannerAd::findOrFail($id);
        $ad->title=$request->title;
        $ad->link = $request->link;
        $ad->expiry_date = $request->expiry_date;

        if($request->hasFile('image')){
            unlink(public_path('bannerad_images/'.$ad->image));
            $photo = $request->file('image');
            $filename = $photo->getClientOriginalName();
            $location = public_path('bannerad_images/');
            $photo->move($location,$filename);
            $ad->image = $filename;
        }
        $ad->save();

        $request->session()->flash('success', 'Banner Ad successfully updated.');        
        
        return redirect()->route('bannerAds.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $ad = BannerAd::findOrFail($id);

        unlink(public_path('bannerad_images/'.$ad->image));
        $ad->delete();
        
        $request->session()->flash('success', 'Banner Ad successfully deleted.');        
        
        return redirect()->route('bannerAds.index');
    }

    public function expiredAds(){
        $date = Carbon::today();  
        $ads = BannerAd::where('expiry_date','<',$date)->orderBy('updated_at','desc')->paginate(10);

        return view('admin.bannerads.result')->with('ads',$ads);
    }
}
