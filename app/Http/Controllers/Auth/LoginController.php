<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\MyBroker;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use App\Mail\ConfirmUserEmail;
use Mail;
use App\User;
use Auth;
use Validator;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request, MyBroker $myBroker)
    {
        $this->validateLogin($request);
        //Login on SSO SERVER
        if($user = $myBroker->loginUser($request->get('email'),$request->get('password'))){
            $validator = Validator::make($user, [
                'verified' => 'accepted'
            ]);
    
            if ($validator->fails()) {
                Session::put($user['email'], $user);
                
                $validator->errors()->add('email', 'Your account is not verified yet. (<a href="'.route('resend_confirmation',array('email' => $user["email"] )).'" style="color:#4545c8">Resend</a> verification email?)');
                return redirect()->to('/login')->withErrors($validator)->withInput();
            }

            // If the class is using the ThrottlesLogins trait, we can automatically throttle
            // the login attempts for this application. We'll key this by the username and
            // the IP address of the client making these requests into this application.
            if ($this->hasTooManyLoginAttempts($request)) {
                $this->fireLockoutEvent($request);

                return $this->sendLockoutResponse($request);
            }

            if ($this->attemptLogin($request)) {
                return $this->sendLoginResponse($request);
            }
        }
        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        $errors = [$this->username() => trans('auth.failed')];
        if ($request->expectsJson()) {
            return response()->json($errors, 422);
        }
        
        return redirect()->to('/login')
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors($errors);
    }

    public function loginProviderUrl($provider, MyBroker $myBroker){
        $url = $myBroker->socialLoginUrl($provider);
        return redirect($url);
    }

    public function handleProviderCallback(Request $request, $provider, MyBroker $myBroker){
        $user = $myBroker->socialLogin($request,$provider);
        
        if($user){
            if(!$user['email']){
                Session::put($user['provider_id'],$user);
                return view('auth.getemail')->with('id',$user['provider_id']);
            }
            $validator = Validator::make($user, [
                'verified' => 'accepted'
            ]);
    
            if ($validator->fails()) {
                Session::put($user['email'], $user);
                
                $validator->errors()->add('email', 'Your account is not verified yet. (<a href="'.route('resend_confirmation',array('email' => $user["email"] )).'" style="color:#4545c8">Resend</a> verification email?)');
                return redirect()->to('/login')->withErrors($validator)->withInput();
            }

            $existing = User::where('email', $user['email'])->first();

            if(!$existing){
                $existing= User::create([
                    'email' => $user['email'],
                ]);
            }
            
            Auth::loginUsingId($existing->id);
            
            if(!$user['about'])
                return redirect()->to(env('ACCOUNT_URL').'completeprofile?redirectUrl='.url('home'));
            else
                return redirect('home');
        }
        else{
            return redirect('login');
        }
    }
    
    public function logout(Request $request)
    {        
        $broker = new MyBroker();
        $broker->logout();
        $this->guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect('/login');
    }

    public function setEmail(Request $request, MyBroker $myBroker){
        
        $socialUser = Session::get($request->id);
        
        $validator = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);

        if ($validator->fails()) {
            $errors = ['email'=>'Please provide a valid email.'];
            return view('auth.getemail')->with('errors',$errors)->with('id',$request->id);
        }

        if($socialUser){
            $result = $myBroker->setEmail($socialUser['provider'],$socialUser['provider_id'],$request->email);

            if($result == -1){
                $errors = ['email'=>'Email has already been taken'];
                return view('auth.getemail')->with('errors',$errors)->with('id',$request->id);
            }
            elseif($result){
                Mail::to($result['email'])->send(new ConfirmUserEmail($result));

                Session::put($result['email'], $result);

                $validator = Validator::make($result, [
                    'verified' => 'accepted'
                ]);
        
                if ($validator->fails()) {
                    $errors = ['email'=>'Please Check your email to verify your account. (<a href="'.route('resend_confirmation',array('email' => $result['email'] )).'" style="color:#4545c8">Resend</a> verification email?)'];
                    return view('auth.getemail')->with('errors',$errors)->with('id',$request->id);
                }
                return view('auth.getemail')->with('id',$request->id);
            }
        }
        return redirect('login');
        
    }

    public function confirmEmail($token){
        if(Auth::check())
            return redirect()->route('home');
        
        $broker = new MyBroker;
        if($result = $broker->confirmUser($token)){
            
            if(User::where('email',$result['email'])->exists()){
                $errors = new MessageBag;
                $errors->add('email', 'Your account is already activated.');
                return redirect('login')->withErrors($errors)->withInput();
            }
            $user= User::create([
                'email' => $result['email'],
            ]);

            Auth::loginUsingId($user->id);

            return redirect()->to(env('ACCOUNT_URL').'completeprofile?redirectUrl='.url('home'));
        }else{
            $errors = new MessageBag;
            $errors->add('email', 'The code is invalid');
            return redirect('login')->withErrors($errors)->withInput();
        }

        return redirect()->route('login');
    }
}
