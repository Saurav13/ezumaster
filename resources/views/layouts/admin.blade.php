@include('admin.partials.header')
@include('admin.partials.navbar')
@yield('body')
@include('admin.partials.footer')
